package com.ifast.core;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

//@UtilityClass
public class Wrapperz {
/*
    public static <T> EntityWrapper<T> entityWrapper(Class<T> clz) {
        return new EntityWrapper<T>();
    }
    public static <T> EntityWrapper<T> entityWrapper(Class<T> clz, T obj) {
        return new EntityWrapper<T>(obj);
    }

    public static <T> UpdateWrapper<T> update(Class<T> clz) {
        return new UpdateWrapper<T>();
    }
    */


    public static <T> QueryWrapper<T> query(Class<T> clz) {
        return Wrappers.<T>query();
        // return new QueryWrapper<T>();

    }

    public static <T> UpdateWrapper<T> update(Class<T> clz) {
        return new UpdateWrapper<T>();
    }
}
