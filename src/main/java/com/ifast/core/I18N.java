package com.ifast.core;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class I18N {

    private static MessageSource messageSource;

    public I18N(MessageSource messageSource) {
        I18N.messageSource = messageSource;
    }

    public static String get(String code, Object... args) {
        try {
            return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
        } catch (Exception e) {
            return code;
        }
    }
}
