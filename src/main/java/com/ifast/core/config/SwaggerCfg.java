package com.ifast.core.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.SpringfoxWebMvcConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Amber L
 * @version v0.0.1
 * @desc
 * @date 2021/1/5 13:53
 */
@Slf4j
@EnableSwagger2
@EnableSwaggerBootstrapUI
@Configuration
@Order(1000)
@ConditionalOnClass(SpringfoxWebMvcConfiguration.class)
public class SwaggerCfg implements WebMvcConfigurer {



  @Bean
  public Docket defaultAPI() {
    return createRestApi("_default", "/**/**");
  }

  @Bean
  public Docket apiAnonInterfAPI() {
    return createRestApi("api_anonInterf", "/anonInterf/**");
  }
  @Bean
  public Docket apiOpenAPI() {
    return createRestApi("api_open", "/open/**");
  }

  @Bean
  public Docket commonAPI() {
    return createRestApi("common", "/common/**");
  }

  @Bean
  public Docket coreAPI() {
    return createRestApi("core", "/core/**");
  }

  @Bean
  public Docket sysAPI() {
    return createRestApi("sys", "/sys/**");
  }

  @Bean
  public Docket usersAPI() {
    return createRestApi("users", "/users/**");
  }

  @Bean
  public Docket expressAPI() {
    return createRestApi("express", "/express/**");
  }

  @Bean
  public Docket prodAPI() {
    return createRestApi("prod", "/prod/**");
  }

  @Bean
  public Docket sampleAPI() {
    return createRestApi("sample", "/sample/**");
  }

  @Bean
  public Docket vpayAPI() {
    return createRestApi("vpay", "/vpay/**");
  }

  @Bean
  public Docket wxAPI() {
    return createRestApi("wx", "/wx/**");
  }

  @Bean
  public Docket demoAPI() {
    return createRestApi("demo", "/demo/**");
  }

  @Bean
  public Docket loginAPI() {
    return createRestApi("login", "/login/**");
  }


  @Bean
  public Docket orderAPI() {
    return createRestApi("order", "/order/**");
  }

  @Bean
  public Docket bomAPI() {
    return createRestApi("bom", "/bom/**");
  }

  @Bean
  public Docket bohAPI() {
    return createRestApi("boh", "/boh/**");
  }

  @Bean
  public Docket docketRestApi() {
    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo())
        .select()
        //.apis(RequestHandlerSelectors.withClassAnnotation(Api.class)) //, RestController.class, Controller.class, ControllerAdvice.class
        .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
        .paths(PathSelectors.any())
        .build()
        .groupName("_old_config_api")
        ;
  }


  public Docket createRestApi(String groupName, String pathSelector) {
    return new Docket(DocumentationType.SWAGGER_2)
        .enable(true)
        .useDefaultResponseMessages(false)
        .apiInfo(apiInfo())
        .select()
        .apis(RequestHandlerSelectors.any())
        //.paths(PathSelectors.any())
        .paths(PathSelectors.ant(pathSelector))
        .build()
        .groupName(groupName)
        .pathMapping("/")
        ;
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        // 页面标题
        .title("bigerp")
        .termsOfServiceUrl("http://127.0.0.1:8088")
        // 创建人
        .contact(new Contact("author", "url", "email"))
        // 版本号
        .version("v1.0.2.x")
        // 描述
        .description("ERP docs")
        .build();

  }

  @Override
  public void addViewControllers(ViewControllerRegistry registry) {

  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    log.info("add swagger2 UI");
    registry.addResourceHandler("/doc.html").addResourceLocations("classpath:/META-INF/resources/");
    registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
    registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");

  }

}
